"""Package to provide code for demonstration of Continuous Deployment pipeline smells.""" 

from ._service import app, start_service

__all__ = [
    'app',
    'start_service'
]
