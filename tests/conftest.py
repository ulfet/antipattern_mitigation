"""Fixture for mocking entities for unit-testing `service`."""
import sqlite3 as sql
import os
import pytest
import glob


@pytest.fixture
def mock_db():
	"""Mock database for unit-testing `service`."""
	test_db_path = './tests/test_db.db'
	test_db_schema = './tests/test_schema.sql'

	connection = sql.connect(test_db_path)
	cursor = connection.cursor()

	with open(test_db_schema) as schema:
		cursor.executescript(schema.read())
		
	connection.commit()
	
	yield connection

	connection.close()

	os.remove(test_db_path) 