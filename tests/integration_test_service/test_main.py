from antipattern_mitigation.__main__ import HOST, PORT


def test_service_server_settings() -> None:
    assert HOST == 'localhost'
    assert PORT == 9999
    pass