from fastapi import FastAPI
from antipattern_mitigation.service._service import app, get_hello, get_game_by_id # noqa: I100

def test_get_game_by_id():
	game_id = 1
	response = get_game_by_id(game_id)
	assert response['ID'] == game_id
	assert response['NAME'] == 'Final Fantasy VII'
	assert response['GENRE'] == 'JRPG'
	assert response['PUBLICATION_YEAR'] == 1997

def test_get_hello():
	response = get_hello()
	assert response['message'] == 'Hello World'