from fastapi import FastAPI
from antipattern_mitigation.service._service import app, get_hello, get_game_by_id # noqa: I100

def test_get_hello():
	response = get_hello()
	assert response['message'] == 'Hello World'


def test_get_game_by_id(mock_db, monkeypatch):
	game_id = 1
	response = get_game_by_id(game_id, mock_db)
	assert response['ID'] == game_id
	assert response['NAME'] == 'test_game_name'
	assert response['GENRE'] == 'test_game_genre'
	assert response['PUBLICATION_YEAR'] == 1990