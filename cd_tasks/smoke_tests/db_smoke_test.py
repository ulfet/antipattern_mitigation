import sqlite3
import os
import glob


_DB_PATH = '../db/latest/latest.db'


print(f'[Smoke Test / DB]: start')

if not os.path.isfile(_DB_PATH):
	raise RuntimeError(f'db smoke test failed, file not exists: {_DB_PATH}')

print(f'[Smoke Test / DB]: DB file find successful: {_DB_PATH}')

try:
	db_connection = sqlite3.connect(_DB_PATH)
except sqlite3.OperationalError as err:
	raise RuntimeError(f'db smoke test failed, {err}')

print(f'[Smoke Test / DB]: DB file connection successful: {_DB_PATH}')