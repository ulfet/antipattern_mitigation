"""DB Migrator Module used in Continuous Deployment."""
import sqlite3
import glob

_SCHEMA_PATH = 'schema.sql'
_DB_PATH = '../latest/latest.db'

# print(glob.glob('*'))

class DBMigrator:
	"""Enables forward and backward rolling of a DB schema."""

	def __init__(self, schema_path: str, db_path: str) -> None:
		self.schema_path = schema_path
		self.db_path = db_path

	def roll_backward(self) -> None:
		"""Roll a DB backward."""
		raise RuntimeError('This is the base DB(#0), cannot go back')

	def roll_forward(self) -> None:
		"""Roll a DB forward."""
		# no previous DB exists
		# just insert

		connection = sqlite3.connect(self.db_path)

		cursor = connection.cursor()
		with open(self.schema_path, 'r') as schema:
			cursor.executescript(schema.read())
		
		connection.commit()
		connection.close()