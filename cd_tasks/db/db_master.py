"""Module that enable the database migration."""
import os
import shutil
import glob
import importlib
from typing import List

_SCHEMA_NAME = 'schema.sql'
_SCRIPT_NAME = '.db_migrator'
_DB_FOLDER = './latest' 
_DB_PATH = './latest/latest.db'


def migrate_db_to_latest() -> None:
	"""Migrate the database to its final revision."""
	if os.path.exists(_DB_FOLDER):
		shutil.rmtree(_DB_FOLDER)

	db_folders = glob.glob('./**/')
	db_scripts: List[str] = [db_folder[2:-1] + _SCRIPT_NAME for db_folder in db_folders]
	db_schemas = [db_folder[:] 	  + _SCHEMA_NAME for db_folder in db_folders]

	if not os.path.exists(_DB_FOLDER):
		os.makedirs(_DB_FOLDER)

	for db_script, db_schema in zip(db_scripts, db_schemas):
		print(f'Applying db migration: {db_script}, with db schema: {db_schema}' )
		imported_script = importlib.import_module(db_script, package=None)

		db_migrator = imported_script.DBMigrator(db_schema, _DB_PATH) # type: ignore
		db_migrator.roll_forward()
		print(f'    success')

migrate_db_to_latest()